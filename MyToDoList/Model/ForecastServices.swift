//
//  ForecastService.swift
//  MyToDoList
//
//  Created by bwc on 25/05/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import Foundation
import Alamofire

class ForecastServices {
    
    //sample url : https://jsonplaceholder.typicode.com/todos/1
    
    let forecastBaseURL = URL(string: "https://jsonplaceholder.typicode.com/todos")
    
    func getUserTodos(id: Int, completion: @escaping (Todo?) -> Void)
    {
        if let forecastURL = URL(string: "\(forecastBaseURL!)/\(id)") {
            Alamofire.request(forecastURL).responseJSON(completionHandler: { (response) in
                
                if (response.result.isSuccess) {
                    
                    
                    if let jsonDictionary = response.result.value as? [String : Any]
                        {
                    
                                let todo = Todo(todoDictionary: jsonDictionary as [String: Any])
                            
                                completion(todo)
                            
 
                    
                    } else {
                        completion(nil)
                    }
                } else if (response.result.isFailure) {
                    //Manager your error
                    switch (response.error!._code){
                    case NSURLErrorTimedOut:
                        //Manager your time out error
                        print("Network Timeout !")
                        break
                    case NSURLErrorNotConnectedToInternet:
                        //Manager your not connected to internet error
                        print("You're not connected to Internet")
                        break
                    default:
                        //manager your default case
                        print(response.error!._code)
                    }
                }
                
                
                
            })
        }
        
    } // end getUserTodos
    
    func postUserToDo(todoTitle: String) -> Void
    {
        
    
    let newTodo: [String: Any] = ["title": todoTitle, "completed": 0, "userId": 1]
        
        Alamofire.request(forecastBaseURL!, method: .post, parameters: newTodo, encoding: JSONEncoding.default)
    .responseJSON { response in
    
        guard response.result.error == nil else {
            // got an error in getting the data, need to handle it
            print("error calling POST on /todos/1")
            print(response.result.error!)
            return
        }
        // make sure we got some JSON since that's what we expect
        guard let json = response.result.value as? [String: Any] else {
            print("didn't get todo object as JSON from API")
            print("Error: \(String(describing: response.result.error))")
            return
        }
        // get and print the title
        guard let todoTitle = json["title"] as? String else {
            print("Could not get todo title from JSON")
            return
        }
        
    print("The title is: " + todoTitle)
        }
    }
    
    func todoDelete(id: Int) {
        let todoToDelete =  URL(string: "\(forecastBaseURL!)/\(id)")
        Alamofire.request(todoToDelete!, method: .delete)
            .responseJSON { response in
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    print("error calling DELETE on /todos/1")
                    print(response.result.error!)
                    return
                }
                print("DELETE ok")
        }
    }
}
