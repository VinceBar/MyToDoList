//
//  Todo.swift
//  MyToDoList
//
//  Created by bwc on 25/05/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import Foundation

class Todo
{
    let title: String?
    let completed: Int?
    let id: Int?
    let userId: Int?
    
    struct todoKeys {
        static let title = "title"
        static let completed = "completed"
        static let id = "id"
        static let userId = "userId"
    }
    
    init (todoDictionary: [String: Any]) {
        title = todoDictionary[todoKeys.title] as? String
        completed = todoDictionary[todoKeys.completed] as? Int
        id = todoDictionary[todoKeys.id] as? Int
        userId = todoDictionary[todoKeys.userId] as? Int
    }
}
