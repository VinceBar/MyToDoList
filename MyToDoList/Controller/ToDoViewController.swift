//
//  To-Do-ViewController.swift
//  MyToDoList
//
//  Created by bwc on 25/05/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import UIKit

class ToDoViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{

    @IBOutlet weak var todoTableView: UITableView!
    @IBOutlet weak var tableView: UITableView!
    
    var id: Int = 0
    var idMax: Int = 20
    var forecastServices: ForecastServices!
    
    var todos = [Todo]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.dataSource = self
        tableView.delegate = self
        
        for i in 0 ... idMax {
            loadUserToDoList(id: i)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for i in 0 ... idMax {
            loadUserToDoList(id: i)
        }
    }
  
    
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return the number of rows
        return todos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCellIdentifier", for: indexPath) as! ToDoTableViewCell
        
        let entry = todos[(indexPath as NSIndexPath).row]
        let todoTitle = entry.title
        
        cell.dodoCellLabel.text = todoTitle
        
        return cell
        
    }
    
    // MARK: Delegates

    // MARK: UITableViewDelegate Methods
    // For deleting
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            deleteUserToDo(id: indexPath.row)
            
           
        }
    }

    //MARK: Utilities
    private func loadUserToDoList(id: Int) -> Void {
        forecastServices = ForecastServices()
        forecastServices.getUserTodos(id: id)
        { (todo) in
            if let todo = todo {
                DispatchQueue.main.async {
                    
                    self.todos.append(todo)
                    
                   
                    self.tableView.reloadData()
                    
                }
            }
        }
    }
    
    private func deleteUserToDo(id: Int) -> Void {
        forecastServices = ForecastServices()
        forecastServices.todoDelete(id: id)
    }
    
}
