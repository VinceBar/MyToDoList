//
//  Add-To-Do-ViewController.swift
//  MyToDoList
//
//  Created by bwc on 25/05/2018.
//  Copyright © 2018 bwc. All rights reserved.
//

import UIKit

class AddToDoViewController: UIViewController {
    
    @IBOutlet weak var todoTextView: UITextView!
    
    var forecastServices: ForecastServices!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK/ Actions
    @IBAction func addTodoButton(_ sender: UIButton) {
        postUserToDo(text: todoTextView.text)
    }
    
    private func postUserToDo(text: String) -> Void {
        forecastServices = ForecastServices()
        forecastServices.postUserToDo(todoTitle: text) 
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
